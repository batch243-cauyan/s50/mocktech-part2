let collection = [];

// Write the queue functions below.

function print() {
    // It will show the array
    return collection;
}

function enqueue(element) {
    //In this funtion you are going to make an algo that will add an element to the array
    // Mimic the function of push method


    let lastElement = collection.length;
    collection[lastElement]=element;
    return collection;

}


function dequeue() {
 

    if (collection.length === 0) {
        return undefined;
      }
      let first = collection[0];
      for (let i = 0; i < collection.length; i++) {
        collection[i] = collection[i + 1];
      }
      collection.length = collection.length - 1;
      return collection;

}



function front() {
    // In here, you are going to catch the first element
    if(collection.length===0){
        return collection;
    }

    return collection[0];   

}

// starting from here, di na pwede gumamit ng .length property
function size() {
     // Number of elements   
    let count =0;
    while(collection[count]!==undefined){
        count++;
    }
    // do {
        
    //     count++
    // } while (collection[count]!==undefined);
    return count;

}

function isEmpty() {
    //it will check whether the function is empty or not
    if (collection[0] === undefined) {
        return true;
      } else {
        return false;
      }

}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};